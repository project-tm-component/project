<section class="licenseList">
    <div class="wrapper">
        <div class="article">
<? foreach ($arResult['LICENSES'] as $list) { ?>
            <div class="headLine cleaFix">
                <?=$list['DESCRIPTION'] ?>
            </div><!-- /headLine -->
            <ul>
    <? foreach ($list['LIST'] as $value) { ?>
                <li><a href="<?=$value['FILE']?>">
                        <img src="<?=$value['DETAIL_PICTURE']?>" alt="<?=$value['NAME']?>">
                    </a></li>
<? } ?>
            </ul>
<? } ?>
           
        </div><!-- /article -->

    </div><!-- /wrapper -->
</section>