<?php

CBitrixComponent::includeComponentClass('project:component');

use Bitrix\Iblock,
    Bitrix\Main;

class projectLicenses extends projectComponent {

    protected function projectGetCacheKeys() {
        return array('LICENSES');
    }

    public function executeComponent() {
        if ($this->projectCache()) {
            if (self::projectLoader('iblock', 'jerff.coralina')) {
                $arSort = array('LEFT_MARGIN' => 'ASC');
                $arFilter = array(
                    'IBLOCK_ID' => Jerff\Coralina\Config::LICENSES_IBLOCK_ID,
                    'GLOBAL_ACTIVE' => 'Y',
                    'ACTIVE' => 'Y'
                );
                $arSelect = array('ID', 'ISECTION_ID', 'NAME', 'UF_EN_NAME', 'UF_EN_NOTICE', 'DESCRIPTION');
                $res = CIBlockSection::GetList($arSort, $arFilter, false, $arSelect);
                $this->arResult['LICENSES'] = array();
                while ($arItem = $res->Fetch()) {
                    if (Jerff\Coralina\Lang::isEn()) {
                        if (!empty($arItem['UF_EN_NAME'])) {
                            $arItem['NAME'] = $arItem['UF_EN_NAME'];
                        }
                        if (!empty($arItem['UF_EN_NOTICE'])) {
                            $arItem['DESCRIPTION'] = $arItem['UF_EN_NOTICE'];
                        }
                    }
                    $this->arResult['LICENSES'][$arItem['ID']] = array(
                        'NAME' => $arItem['NAME'],
                        'DESCRIPTION' => $arItem['DESCRIPTION'],
                    );
                }

                $arSort = array('SORT' => 'ASC', 'NAME', 'ASC');
                $arFilter = array(
                    'IBLOCK_ID' => Jerff\Coralina\Config::LICENSES_IBLOCK_ID,
                    'SECTION_ID' => array_keys($this->arResult['LICENSES']),
                    'ACTIVE' => 'Y',
                    '!DETAIL_PICTURE' => 0,
                );
                $arSelect = array('IBLOCK_ID', 'IBLOCK_SECTION_ID', 'DETAIL_PICTURE', 'NAME', 'PROPERTY_FILE', 'PROPERTY_EN_NAME');
                $res = CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);
                $this->arResult['ITEMS'] = array();
                while ($arItem = $res->Fetch()) {
                    if (Jerff\Coralina\Lang::isEn()) {
                        if (!empty($arItem['UF_EN_NAME'])) {
                            $arItem['NAME'] = $arItem['UF_EN_NAME'];
                        }
                    }
                    if (!empty($arItem['PROPERTY_FILE_VALUE'])) {
                        $arItem['FILE'] = cFile::GetPath($arItem['PROPERTY_FILE_VALUE']);
                    } else {
                        $arItem['FILE'] = cFile::GetPath($arItem['DETAIL_PICTURE']);
                    }
                    $this->arResult['LICENSES'][$arItem['IBLOCK_SECTION_ID']]['LIST'][] = array(
                        'NAME' => $arItem['NAME'],
                        'DETAIL_PICTURE' => cFile::GetPath($arItem['DETAIL_PICTURE']),
                        'FILE' => $arItem['FILE'],
                    );
                }

                $this->projectResultCacheKeys();
                $this->projectTemplate();
            }
        }
        return $this->arResult;
    }

}
