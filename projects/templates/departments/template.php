<? foreach ($arResult['PROJECTS'] as $list) { ?>
    <? if (!empty($list['LIST'])) { ?>
        <div class="departmentHeadline">
            <img src="<? if ($list['PICTURE']) { ?><?= $list['PICTURE'] ?><? } else { ?><?= SITE_TEMPLATE_PATH ?>/markup/images/department_1.jpg<? } ?>" alt="<?= htmlspecialchars($list['NAME']) ?>" />
            <h2><?= $list['NAME'] ?></h2>
        </div><!-- /departmentHeadline -->
        <ul>
            <? foreach ($list['LIST'] as $value) { ?>
                <li><a href="<?= $value['DETAIL_PAGE_URL'] ?>"><?= $value['NAME'] ?></a></li>
            <? } ?>
        </ul>
    <? } ?>
<? } ?>