<div class="centerBlock">
    <? foreach ($arResult['PROJECTS'] as $list) { ?>
        <div class="industry">
            <div class="projectHeadline">
                <img src="<? if ($list['PICTURE']) { ?><?= $list['PICTURE'] ?><? } else { ?><?= SITE_TEMPLATE_PATH ?>/markup/images/department_1.jpg<? } ?>" alt="<?= htmlspecialchars($list['NAME']) ?>" />
                <h4><?= $list['NAME'] ?></h4>
            </div><!-- /projectHeadline -->
            <ul>
                <? foreach ($list['LIST'] as $value) { ?>
                    <li><a href="<?= $value['DETAIL_PAGE_URL'] ?>"><?= $value['NAME'] ?></a></li>
                <? } ?>
            </ul>
        </div><!-- /industry -->
    <? } ?>
</div><!-- /centerBlock -->
