<?php

CBitrixComponent::includeComponentClass('project:component');

use Bitrix\Iblock,
    Bitrix\Main;

class projectProjects extends projectComponent {

    protected function projectGetCacheKeys() {
        return array('PROJECTS');
    }

    public function executeComponent() {
        if ($this->projectCache()) {
            if (self::projectLoader('iblock', 'jerff.coralina')) {
                $arSort = array('LEFT_MARGIN' => 'ASC');
                $arFilter = array(
                    'IBLOCK_ID' => Jerff\Coralina\Config::PROJECTS_IBLOCK_ID,
                    'GLOBAL_ACTIVE' => 'Y',
                    'ACTIVE' => 'Y'
                );
                if (isset($this->arParams['SECTION_CODE'])) {
                    $arFilter['CODE'] = $this->arParams['SECTION_CODE'];
                }
                $arSelect = array('ID', 'NAME', 'UF_EN_NAME', 'PICTURE');
                $res = CIBlockSection::GetList($arSort, $arFilter, false, $arSelect);
                $this->arResult['PROJECTS'] = array();
                while ($arItem = $res->Fetch()) {
                    if (Jerff\Coralina\Lang::isEn()) {
                        if (!empty($arItem['UF_EN_NAME'])) {
                            $arItem['NAME'] = $arItem['UF_EN_NAME'];
                        }
                    }
                    $this->arResult['PROJECTS'][$arItem['ID']] = array(
                        'NAME' => $arItem['NAME'],
                        'PICTURE' => cFile::getPath($arItem['PICTURE']),
                    );
                }

                $arSort = array('ACTIVE_FROM' => 'DESC', 'ID', 'DESC');
                $arFilter = array(
                    'IBLOCK_ID' => Jerff\Coralina\Config::PROJECTS_IBLOCK_ID,
                    'SECTION_ID' => array_keys($this->arResult['PROJECTS']),
                    'ACTIVE' => 'Y'
                );
                if (isset($this->arParams['DEPARTAMENT_ID'])) {
                    $arFilter['PROPERTY_DEPARTNMENT'] = $this->arParams['DEPARTAMENT_ID'];
                }
                $arSelect = array('IBLOCK_ID', 'IBLOCK_SECTION_ID', 'NAME', 'PROPERTY_EN_NAME', 'DETAIL_PAGE_URL');
                $res = CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);
                $res->SetUrlTemplates("", SITE_DIR . 'projects/#SECTION_CODE#/#ELEMENT_ID#/');
                $this->arResult['ITEMS'] = array();
                while ($arItem = $res->GetNext()) {
                    if (Jerff\Coralina\Lang::isEn()) {
                        if (!empty($arItem['PROPERTY_EN_NAME_VALUE'])) {
                            $arItem['NAME'] = $arItem['PROPERTY_EN_NAME_VALUE'];
                        }
                    }
                    $this->arResult['PROJECTS'][$arItem['IBLOCK_SECTION_ID']]['LIST'][] = array(
                        'NAME' => $arItem['NAME'],
                        'DETAIL_PAGE_URL' => $arItem['DETAIL_PAGE_URL'],
                    );
                }

                $this->projectResultCacheKeys();
                $this->projectTemplate();
            }
        }
        if (isset($this->arParams['SECTION_CODE'])) {
            foreach ($this->arResult['PROJECTS'] as $value) {
                global $APPLICATION;
                $APPLICATION->AddChainItem($value['NAME']);
                $APPLICATION->SetPageProperty('title', $value['NAME']);
                $APPLICATION->SetPageProperty('keywords', $value['NAME']);
                $APPLICATION->SetPageProperty('description', $value['NAME']);
            }
        }
        return $this->arResult;
    }

}
