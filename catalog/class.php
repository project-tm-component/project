<?php

CBitrixComponent::includeComponentClass('project:component');

use Bitrix\Iblock,
    Bitrix\Main;

class projectCatalog extends projectComponent {

    protected function projectGetCacheKeys() {
        return array('CATALOG');
    }

    public function executeComponent() {
        if ($this->projectCache()) {
            if (self::projectLoader('iblock', 'jerff.coralina')) {
                $arSort = array('LEFT_MARGIN' => 'ASC');
                $arFilter = array(
                    'IBLOCK_ID' => Jerff\Coralina\Config::CATALOG_IBLOCK_ID,
                    'GLOBAL_ACTIVE' => 'Y',
                    'SECTION_ID' => '0',
                    'ACTIVE' => 'Y'
                );
                if (isset($this->arParams['SECTION_CODE'])) {
                    $arFilter['CODE'] = $this->arParams['SECTION_CODE'];
                }
                if (isset($this->arParams['DEPARTAMENT_ID'])) {
                    $arFilter['UF_DEPARTAMENT'] = $this->arParams['DEPARTAMENT_ID'];
                }
                $arSelect = array('ID', 'NAME', 'UF_NAME_ENG', 'SECTION_PAGE_URL');
                $res = CIBlockSection::GetList($arSort, $arFilter, false, $arSelect);
                $this->arResult['CATALOG'] = array();
                while ($arItem = $res->GetNext()) {
                    if (Jerff\Coralina\Lang::isEn()) {
                        if (!empty($arItem['UF_NAME_ENG'])) {
                            $arItem['NAME'] = $arItem['UF_NAME_ENG'];
                        }
                    }
                    $this->arResult['CATALOG'][$arItem['ID']] = array(
                        'NAME' => $arItem['NAME'],
                        'URL' => $arItem['SECTION_PAGE_URL']
                    );
                }

                $res = CIBlockSection::GetTreeList(
                                $arFilter = Array('IBLOCK_ID' => Jerff\Coralina\Config::CATALOG_IBLOCK_ID),
                                $arSelect = Array('ID', 'IBLOCK_SECTION_ID', 'DEPTH_LEVEL')
                );
                $tree = array();
                while ($section = $res->Fetch()) {
                    if($section['DEPTH_LEVEL']==1) {
                        $parent = $section['ID'];
                    } else {
                        $tree[$section['ID']] = $parent;
                    }
                }

                $arSort = array('ACTIVE_FROM' => 'DESC', 'ID', 'DESC');
                $arFilter = array(
                    'IBLOCK_ID' => Jerff\Coralina\Config::CATALOG_IBLOCK_ID,
                    'SECTION_ID' => array_keys($this->arResult['CATALOG']),
                    'PROPERTY_IS_MAIN' => 1,
                    'INCLUDE_SUBSECTIONS' => 'Y',
                    'ACTIVE' => 'Y'
                );
                $arSelect = array('IBLOCK_ID', 'IBLOCK_SECTION_ID', 'NAME', 'PROPERTY_NAME_ENG', 'PREVIEW_PICTURE', 'DETAIL_PICTURE', 'DETAIL_PAGE_URL');
                $res = CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);
//                $res->SetUrlTemplates("", SITE_DIR . 'projects/#SECTION_CODE#/#ELEMENT_ID#/');
                $this->arResult['ITEMS'] = array();
                while ($arItem = $res->GetNext()) {
                    if (Jerff\Coralina\Lang::isEn()) {
                        if (!empty($arItem['PROPERTY_NAME_ENG_VALUE'])) {
                            $arItem['NAME'] = $arItem['PROPERTY_NAME_ENG_VALUE'];
                        }
                    }
                    $file = $arItem['PREVIEW_PICTURE'] ?: $arItem['DETAIL_PICTURE'];
                    $this->arResult['CATALOG'][$tree[$arItem['IBLOCK_SECTION_ID']]]['LIST'][] = array(
                        'NAME' => $arItem['NAME'],
                        'IMAGE' => cFile::GetPath($file),
                        'DETAIL_PAGE_URL' => $arItem['DETAIL_PAGE_URL'],
                    );
                }

                $this->projectResultCacheKeys();
                $this->projectTemplate();
            }
        }
        return $this->arResult;
    }

}
