<section class="tools mainPage">
    <div class="wrapper">
        <div class="headLine withLink cleaFix">
            <?= Jerff\Coralina\Lang::include('main/catalog/header') ?>
            <a href="<?= SITE_DIR ?>catalog/" class="futherButton" style="top: 25px;"><?= Jerff\Coralina\Lang::include('main/catalog/link') ?> <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
        </div><!-- /headLine -->

        <? foreach ($arResult['CATALOG'] as $list) { ?>
            <? if (!empty($list['LIST'])) { ?>
                <div class="headLine">
                    <h3><a href="<?= $list['URL'] ?>"><?= $list['NAME'] ?> > </a></h3>
                </div><!-- /headLine -->
                <div class="slider twoRows">
                    <? foreach ($list['LIST'] as $list2) { ?>
                        <div>
                            <div class="row">
                                <? foreach ($list2 as $item) { ?>
                                    <a href="<?= $item['DETAIL_PAGE_URL'] ?>">
                                        <img src="<?= $item['IMAGE'] ?>" alt="<?= htmlspecialchars($item['NAME']) ?>" />
                                        <span><?= $item['NAME'] ?></span>
                                    </a>
                                <? } ?>
                            </div><!-- /row -->
                        </div><!-- /slide -->
                    <? } ?>
                </div><!-- /slider -->
            <? } ?>
        <? } ?>
    </div><!-- /wrapper -->
</section><!-- /tools -->