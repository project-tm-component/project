<div class="tools">
    <div class="headLine withLink cleaFix">
        <?= Jerff\Coralina\Lang::include('departments/catalog/header') ?>
        <a href="<?= SITE_DIR ?>catalog/" class="futherButton"><?= Jerff\Coralina\Lang::include('departments/catalog/link') ?> <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
    </div><!-- /headLine -->

    <? foreach ($arResult['CATALOG'] as $list) { ?>
        <? if (!empty($list['LIST'])) { ?>
            <div class="slider twoRows">
                <? foreach ($list['LIST'] as $list2) { ?>
                    <div>
                        <div class="row">
                            <? foreach ($list2 as $index => $item) { ?>
                                <? if ($index == 3) { ?>
                                </div><div class="row">
                                <? } ?>
                                <a href="<?= $item['DETAIL_PAGE_URL'] ?>">
                                    <img src="<?= $item['IMAGE'] ?>" alt="<?= htmlspecialchars($item['NAME']) ?>" />
                                    <span><?= $item['NAME'] ?></span>
                                </a>
                            <? } ?>
                        </div><!-- /row -->
                    </div>
                <? } ?>
            </div><!-- /slider -->
        <? } ?>
    <? } ?>
</div>