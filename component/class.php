<?php

use Bitrix\Main\Loader,
    Bitrix\Iblock;

class projectComponent extends CBitrixComponent {

    protected $arFilter = array();
    protected $isFilterByName = false;

    protected function projectGetCacheKeys() {
        return array();
    }

    protected function projectResultCacheKeys() {
        $this->setResultCacheKeys($this->projectGetCacheKeys());
    }

    static protected function projectLoader(...$arParam) {
        foreach ($arParam as $key) {
            if (!Loader::includeModule($key)) {
                return false;
            }
        }
        return true;
    }

    protected function projectFilterParam(...$arParam) {
        foreach ($arParam as $key) {
            switch ($key) {
                case 'cache':
                    if (empty($this->arParams['CACHE_TIME'])) {
                        $this->arParams['CACHE_TIME'] = 36000000;
                    }
                    $this->arParams['CACHE_GROUPS'] = $this->arParams['CACHE_GROUPS'] ?: 'N';
                    if ($this->arParams['CACHE_GROUPS'] !== 'N') {
                        $this->arParams['CACHE_GROUPS'] = 'Y';
                    }
                    break;

                case 'iblock':
                    $this->arParams['IBLOCK_TYPE'] = trim($this->arParams['IBLOCK_TYPE']);
                    if (empty($this->arParams['IBLOCK_TYPE'])) {
                        $this->arParams['IBLOCK_TYPE'] = 'news';
                    }
                    $this->arParams['IBLOCK_ID'] = (int) $this->arParams['IBLOCK_ID'];
                    break;

                case 'section':
                    $this->arParams['PARENT_SECTION'] = (int) $this->arParams['PARENT_SECTION'];
                    $this->arParams['PARENT_SECTION_CODE'] = $this->arParams['PARENT_SECTION_CODE'] ?: '';
                    $this->arParams['INCLUDE_SUBSECTIONS'] = $this->arParams['INCLUDE_SUBSECTIONS'] != 'N';
                    break;

                case 'count':
                    $this->arParams['NEWS_COUNT'] = (int) $this->arParams['NEWS_COUNT'] ?? 0;
                    break;

                case 'sort':
                    $this->arParams['SORT_BY1'] = trim($this->arParams['SORT_BY1']);
                    if (empty($this->arParams['SORT_BY1'])) {
                        $this->arParams['SORT_BY1'] = 'ACTIVE_FROM';
                    }
                    if (!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $this->arParams['SORT_ORDER1'])) {
                        $this->arParams['SORT_ORDER1'] = 'DESC';
                    }
                    if (empty($this->arParams['SORT_BY2'])) {
                        $this->arParams['SORT_BY2'] = 'SORT';
                    }
                    if (!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $this->arParams['SORT_ORDER2'])) {
                        $this->arParams['SORT_ORDER2'] = 'ASC';
                    }
                    break;

                case 'filter':
                    if (!empty($this->arParams['FILTER_NAME'])
                            and ! preg_match('/[^a-z_][^a-z0-9_]*$/i', $this->arParams['FILTER_NAME'])
                            and ! empty($GLOBALS[$this->arParams['FILTER_NAME']])) {
                        $this->arFilter = $GLOBALS[$this->arParams['FILTER_NAME']];
                    }
                    break;

                default:
                    break;
            }
        }
    }

    protected function projectCache() {
        $this->projectFilterParam('cache');
        return $this->startResultCache(false, $this->projectCacheID(), $this->projectCachePath());
    }

    protected function projectCacheID() {
        $arParam = $this->arParams;
        $arParam[] = $this->arParams['CACHE_GROUPS'] === 'N' ? false : cUser::GetUserGroupArray();
        return $this->getCacheID($arParam);
    }

    protected function projectCachePath() {
        return false;
    }

    protected function projectGetSort() {
        return array(
            $this->arParams['SORT_BY1'] => $this->arParams['SORT_ORDER1'],
            $this->arParams['SORT_BY2'] => $this->arParams['SORT_ORDER2'],
        );
    }

    protected function projectGetFilter(...$arParam) {
        $arSort = $this->projectGetSort();
        $arFilter = array();
        foreach ($arParam as $key) {
            switch ($key) {
                case 'active':
                    $arFilter['ACTIVE'] = 'Y';
                    break;

                case 'iblock':
                    $arFilter['IBLOCK_LID'] = 's1';
                    $arFilter['IBLOCK_TYPE'] = $this->arParams['IBLOCK_TYPE'];
                    $arFilter['IBLOCK_ID'] = $this->arParams['IBLOCK_ID'];
                    break;

                case 'filterByName':
                    if (!empty($this->arParams['FILTER_BY_NAME'])) {
                        foreach ($this->arParams['FILTER_BY_NAME'] as $key => $value) {
                            if (empty($value)) {
                                unset($this->arParams['FILTER_BY_NAME'][$key]);
                            }
                        }
                        if (!empty($this->arParams['FILTER_BY_NAME'])) {
                            $this->isFilterByName = true;
                            $arFilter['NAME'] = $this->arParams['FILTER_NAME'];
                            $arSort = array();
                        }
                    }
                    break;

                case 'section':
                    if (empty($this->isFilterByName)) {
                        $this->arParams['SECTION_ID'] = CIBlockFindTools::GetSectionID(
                                        $this->arParams['PARENT_SECTION'], $this->arParams['PARENT_SECTION_CODE'], array(
                                    'GLOBAL_ACTIVE' => 'Y',
                                    'IBLOCK_ID' => $this->arParams['IBLOCK_ID'],
                                        )
                        );
                        if ($this->arParams['PARENT_SECTION']) {
                            $arFilter['SECTION_ID'] = $this->arParams['PARENT_SECTION'];
                        } elseif ($this->arParams['PARENT_SECTION'] or $this->arParams['PARENT_SECTION_CODE']) {
                            Iblock\Component\Tools::process404(
                                    "", true, true, true, ''
                            );
                        }
                        if ($this->arParams['INCLUDE_SUBSECTIONS']) {
                            $arFilter['INCLUDE_SUBSECTIONS'] = 'Y';
                        }
                    } else {
                        $this->arParams['SECTION_ID'] = $this->arParams['PARENT_SECTION'];
                    }
                    break;

                case 'filter':
                    $arFilter += $this->arFilter;
                    break;

                case 'element':
                    if (empty($this->arParams['ELEMENT_ID'])) {
                        $arFilter['ID'] = CIBlockFindTools::GetElementID(
                                        $this->arParams['ELEMENT_ID'], $this->arParams['~ELEMENT_CODE'], false, false, $arFilter
                        );
                        if (empty($arFilter['ID'])) {
                            Iblock\Component\Tools::process404(
                                    "", true, true, true, ''
                            );
                        }
                    } else {
                        $arFilter['ID'] = $this->arParams['ELEMENT_ID'];
                    }
                    break;

                default:
                    break;
            }
        }
        return array($arSort, $arFilter);
    }

    protected function projectGetSelect() {
        $arSelect = array('ID', 'NAME');
        if (!empty($this->arParams['FIELD_CODE'])) {
            foreach ($this->arParams['FIELD_CODE'] as $value) {
                if ($value) {
                    $arSelect[] = $value;
                }
            }
        }
        if (!empty($this->arParams['PROPERTY_CODE'])) {
            foreach ($this->arParams['PROPERTY_CODE'] as $value) {
                if ($value) {
                    $arSelect[] = 'PROPERTY_' . $value;
                }
            }
        }
        return array_unique($arSelect);
    }

    protected function projectGetKeyID() {
        return $this->isFilterByName ? 'NAME' : 'ID';
    }

    protected function projectResultSort(&$arResult) {
        if ($this->isFilterByName) {
            $arData = array();
            foreach ($this->arParams['FILTER_NAME'] as $value) {
                if (isset($arResult[$value])) {
                    $arData[$value] = $arResult[$value];
                }
            }
            $arResult = $arData;
        }
    }

    protected function projectGetSelectSection() {
        return array("ID", "NAME", 'UF_NOTICE', 'DESCRIPTION', 'SECTION_PAGE_URL', 'UF_BROWSER_TITLE', 'UF_KEYWORDS', 'UF_META_DESCRIPTION');
    }

    protected function projectInitSection() {
        if (empty($this->arParams['PARENT_SECTION'])) {
            return;
        }
        $arFilter = array(
            "IBLOCK_TYPE" => $this->arParams['IBLOCK_TYPE'],
            "IBLOCK_ID" => $this->arParams['IBLOCK_ID'],
            "ID" => $this->arParams['PARENT_SECTION'],
            "ACTIVE" => "Y",
        );
        $arSelect = $this->projectGetSelectSection();
        $res = CIBlockSection::GetList(array(), $arFilter, false, $arSelect);
        if ($this->arResult['SECTION'] = $res->GetNext()) {
            $ipropValues = new Iblock\InheritedProperty\SectionValues($this->arParams['IBLOCK_ID'], $arItem['ID']);
            $this->arResult['SECTION']['IPROPERTY_VALUES'] = $ipropValues->getValues();

            $this->arResult['SECTION']['PATH'] = array();
            $pathIterator = CIBlockSection::GetNavChain(
                            $this->arResult['IBLOCK_ID'], $this->arResult['ID'], array(
                        'ID', 'NAME', 'SECTION_PAGE_URL'
                            )
            );
            $pathIterator->SetUrlTemplates('', $this->arParams['SECTION_URL']);
            while ($path = $pathIterator->GetNext()) {
                $ipropValues = new Iblock\InheritedProperty\SectionValues($this->arParams['IBLOCK_ID'], $path['ID']);
                $path['IPROPERTY_VALUES'] = $ipropValues->getValues();
                $this->arResult['SECTION']['PATH'][] = $path;
            }
        }
    }

    protected function projectTemplate() {
        $this->includeComponentTemplate();
    }

}
