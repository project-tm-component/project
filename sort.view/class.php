<?php

use Bitrix\Main\Application;

class ProjectSortView extends CBitrixComponent {

    public function executeComponent() {
        $this->arResult['INIT'] = function($name, $map, $default = '') {
            global $APPLICATION;
            $request = Application::getInstance()->getContext()->getRequest();
            $item = $request->get($name);
            if (empty($item) and ! empty($_COOKIE['sort-view-' . $name])) {
                $item = $_COOKIE['sort-view-' . $name];
            }
            if (empty($default)) {
                $default = key($map);
            }
            if (!isset($map[$item])) {
                $item = $default;
            }
            $map[$item]['select'] = true;
            foreach ($map as $key => &$value) {
                $value['url'] = $APPLICATION->GetCurPageParam(($name != 'view' and $default == $key) ? '' : $name . '=' . $key, array($name, 'clear_cache', 'bitrix_include_areas'));
            }
            $map[$item]['key'] = strtoupper($item);
            return array($map[$item], $map);
        };
        $this->includeComponentTemplate();
        return array($this->arResult['itemView'], $this->arResult['itemSort']);
    }

}
