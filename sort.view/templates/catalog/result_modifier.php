<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

$arParamsView = array(
    'card' => array(),
    'line' => array()
);
$arParamsSort = array(
    'price-desc' => array(
        'sort' => array(
//                    'CATALOG_AVAILABLE' => 'DESC',
            'PROPERTY_MINIMUM_PRICE' => 'DESC',
            'PROPERTY_MAXIMUM_PRICE' => 'DESC'
        ),
        'name' => 'цене: Дорогие — Дешевые'
    ),
    'price-asc' => array(
        'sort' => array(
//                    'CATALOG_AVAILABLE' => 'DESC',
            'PROPERTY_MINIMUM_PRICE' => 'ASC',
            'PROPERTY_MAXIMUM_PRICE' => 'ASC'
        ),
        'name' => 'цене: Дешевые — Дорогие',
    ),
//            'img-on' => 'изображению: есть — нет',
//            'img-off' => 'изображению: нет — есть',
    'name-asc' => array(
        'sort' => array(
//                    'CATALOG_AVAILABLE' => 'DESC',
            'NAME' => 'ASC'
        ),
        'name' => 'названию: А — я'
    ),
    'name-desc' => array(
        'sort' => array(
//                    'CATALOG_AVAILABLE' => 'DESC',
            'NAME' => 'DESC'
        ),
        'name' => 'названию: я — А'
    ),
    'availability-desc' => array(
        'sort' => array(
            'CATALOG_QUANTITY' => 'DESC'
        ),
        'name' => 'наличию: много — мало'
    ),
    'availability-asc' => array(
        'sort' => array(
            'CATALOG_QUANTITY' => 'ASC,NULLS'
        ),
        'name' => 'наличию: мало — много'
    ),
    'date-desc' => array(
        'sort' => array(
            'CREATED' => 'ASC'
        ),
        'name' => 'дате: новые — старые'
    ),
    'date-asc' => array(
        'sort' => array(
            'CREATED' => 'ASC'
        ),
        'name' => 'дате: старые — новые'
    ),
    'rating-asc' => array(
        'sort' => array(
            'PROPERTY_RATING' => 'ASC,NULLS'
        ),
        'name' => 'рейтингу: Хорошие — Плохие'
    ),
    'rating-desc' => array(
        'sort' => array(
            'PROPERTY_RATING' => 'DESC'
        ),
        'name' => 'рейтингу: Плохие — Хорошие'
    ),
    'comment-desc' => array(
        'sort' => array(
            'PROPERTY_FORUM_MESSAGE_CNT' => 'DESC'
        ),
        'name' => 'количеству отзывов: Много — Мало'
    ),
    'comment-asc' => array(
        'sort' => array(
            'PROPERTY_FORUM_MESSAGE_CNT' => 'ASC,NULLS'
        ),
        'name' => 'количеству отзывов: Мало — Много'
    )
);
if (isset($arParams['SORT'])) {
    $arParamsSort = array_merge(array(
        'search' => $arParams['SORT']
            ), $arParamsSort);
}
list($arResult['itemView'], $arResult['arView']) = self::initParam('view', $arParamsView);
list($arResult['itemSort'], $arResult['arSort']) = self::initParam('sort', $arParamsSort, 'availability-desc');
