<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

$arParamsView = array(
    'card' => array(),
    'line' => array()
);
$arParamsSort = array(
    'name-asc' => array(
        'sort' => array(
            'NAME' => 'ASC'
        ),
        'name' => 'По названию: А — я'
    ),
    'name-desc' => array(
        'sort' => array(
            'NAME' => 'DESC'
        ),
        'name' => 'По названию: я — А'
    ),
    'date-desc' => array(
        'sort' => array(
            'CREATED' => 'DESC'
        ),
        'name' => 'Сначало новые'
    ),
    'date-asc' => array(
        'sort' => array(
            'CREATED' => 'ASC'
        ),
        'name' => 'Старые старые'
    )
);
if (isset($arParams['SORT'])) {
    $arParamsSort = array_merge(array(
        'search' => $arParams['SORT']
            ), $arParamsSort);
}
list($arResult['itemView'], $arResult['arView']) = $arResult['INIT']('view', $arParamsView);
list($arResult['itemSort'], $arResult['arSort']) = $arResult['INIT']('sort', $arParamsSort, 'date-desc');
