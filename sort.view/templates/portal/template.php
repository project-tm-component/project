<div class="feed-item-wrap">
    <div class="feed-post-block feed-post-block-new tm_news_main">
        <div class="bx-newslist-container">
            <div class="bx-newslist-block">
                <div class="col-md-2 bx-newslist-block-img">
                </div>
                <div class="sorting__news-block">
                    <div class="sorting__text">Сортировать новость</div>
                    <div class="sotring__select-block ">
                        <select name="sort" class="sorting__news-block-js" onchange="window.location.href = $(this).val();">
                            <? foreach ($arResult['arSort'] as $key => $value) { ?>
                                <option value="<?= $value['url'] ?>" <? if (isset($value['select'])) { ?>selected="selected"<? } ?>><?= $value['name'] ?></option>
                            <? } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>